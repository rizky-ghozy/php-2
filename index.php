<?php
    require_once('sheep.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal("shaun");
    echo "nama : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "cold_blooded : " . $sheep->cold_blooded . "<br><br>";

    $frog = new frog( "buduk");
    echo "nama : " . $frog->name . "<br>";
    echo "Legs : " . $frog->legs . "<br>";
    echo "cold_blooded : " . $frog->cold_blooded . "<br>";
    echo $frog->jump("Hop Hop") . "<br><br>";


    $ape = new ape( "kera sakti");
    echo "nama : " . $ape->name . "<br>";
    echo "Legs : " . $ape->legs . "<br>";
    echo "cold_blooded : " . $ape->cold_blooded . "<br>";
    echo $ape->yell("Auooo");

?>